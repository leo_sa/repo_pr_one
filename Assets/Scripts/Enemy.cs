﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class Enemy : Entity, IDamagable
    {
        [SerializeField]
        int currentHealth;

        public int CurrentHealth
        {
            set { currentHealth = value; }
            get { return currentHealth; }
        }

        [SerializeField]
        int attackAmount;

        public int AttackAmount
        {
            set { attackAmount = value; }
            get { return attackAmount; }
        }

        [SerializeField]
        bool isAlive;

        public bool IsAlive
        {
            set { isAlive = value; }
            get { return isAlive; }
        }

        public void Damage(int amountOfDamage)
        {
            if(currentHealth > 0)
            {
                currentHealth -= amountOfDamage;

                if(currentHealth <= 0)
                {
                    currentHealth = 0;
                    isAlive = false;
                    Log(EntityName + currentHealth);
                    return;
                }
            }

            else

            {
                currentHealth = 0;
                isAlive = false;
                Log(EntityName + currentHealth);
                return;
            }
      
                
            
            Log("The " + EntityName + "has been Damaged! It stumbles with only " + currentHealth + "HP left");
        }
    }

}
