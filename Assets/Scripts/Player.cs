﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace PROne
{
    public class Player : Entity, IAttack
    {

        [SerializeField]
        Enemy enemyEntity;

        public Enemy EnemyEntity
        {
            set { enemyEntity = value; }
            get { return enemyEntity; }
        }
        [SerializeField]
        int currentHealth;

        public int CurrentHealth
        {
            set { currentHealth = value; }
            get { return currentHealth; }
        }

        [SerializeField]
        int attackAmount;

        public int AttackAmount
        {
            set { attackAmount = value; }
            get { return attackAmount; }
        }

        [SerializeField]
        bool isAlive;

        public bool IsAlive
        {
            set { isAlive = value; }
            get { return isAlive; }
        }

        public void AttackEnemy(Enemy enemy, int attackDamage)
        {
            if(enemy.IsAlive)
            {
                enemy.Damage(attackDamage);
            }

        }

        private void Start()
        {
            Log("Your name is " +EntityName);
            Log("With a massive amount of health: " + currentHealth);
            Log("Dealing incredible Damage: " + attackAmount);
            AttackEnemy(EnemyEntity, attackAmount);
        }


    }
}

