﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class EntityManager : MonoBehaviour, ILog
    {
        
       public void Log(object message)
       {
            Debug.Log(message);
       }

        public Entity[] entities;

        private void Start()
        {
            Log("the names of the entities are being registered");

            for (int i = 0; i < entities.Length; i++)
            {

                Log(i + entities[i].EntityName);

            }
          
        }

        
    }
}

